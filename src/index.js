import * as d3 from 'd3';


var fullwidth = 800;
var fullheight = 500;
var margin = {top: 60, right: 50, bottom: 20, left: 50};

var width = fullwidth - margin.left - margin.right;
var height = fullheight - margin.top - margin.bottom;

var widthScale = d3.scaleLinear()
                   .range([margin.left, width + margin.left]);
var heightScale = d3.scaleBand()
                    .range([margin.top, height + margin.top]);
var color = d3.scaleOrdinal(d3.schemeCategory10);


var xAxis = d3.axisBottom()
              .scale(widthScale);
var yAxis = d3.axisLeft()
              .scale(heightScale);

var svg = d3.select("body")
            .append("svg")
            .attr("width", fullwidth)
            .attr("height", fullheight);

// process the data
var data = [];
for (var i = 0; i < 100; i++) {
  data.push({'a':Math.random()});
  data.push({'b':Math.random()});
  data.push({'c':Math.random()});
  data.push({'d':Math.random()});
  data.push({'e':Math.random()});
  data.push({'f':Math.random()});
  data.push({'g':Math.random()});
}
widthScale.domain([0, d3.max(data.map(x => d3.values(x)[0]))]);
heightScale.domain(data.map(x => d3.keys(x)[0]));

svg.selectAll("circle")
   .data(data)
   .enter()
   .append("circle")
   .attr("cx", d => widthScale(d3.values(d)[0]))
   .attr("cy", d => heightScale(d3.keys(d)[0]) + (Math.random() + 0.5)* heightScale.bandwidth()/2)
   .attr("fill", d => color(d3.keys(d)[0]))
   .attr("r", 5);

svg.selectAll("circle")
   .data(data)
   .enter()
   .append("rect")
   .attr("x", d => widthScale(d3.values(d)[0]))
   .attr("y", d => heightScale(d3.keys(d)[0]) + heightScale.bandwidth()/2);

// add the axes
svg.append("g")
   .attr("class", "axis")
   .attr("transform", "translate(" + 0 + "," + (height + margin.top) + ")")
   .call(xAxis);

svg.append("g")
   .attr("class", "axis")
   .attr("transform", "translate(" + margin.left + ",0)")
   .call(yAxis);
